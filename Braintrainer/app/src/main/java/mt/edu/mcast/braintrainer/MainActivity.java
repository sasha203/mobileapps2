package mt.edu.mcast.braintrainer;

import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView txtvSum;
    TextView txtvResult;

    Button btnScore;
    Button btnTimer;

    Button btnAns0;
    Button btnAns1;
    Button btnAns2;
    Button btnAns3;

    Button btnPlayAgain;

    ArrayList<Integer> answers;
    int locationOfCorrectAnswer;
    int score = 0;
    int numberOfQuestions = 0;

    CountDownTimer countDownTimer;

    boolean finish = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtvSum = findViewById(R.id.txtvSum);
        txtvResult = findViewById(R.id.txtvResult);

        btnScore = findViewById(R.id.btnScore);
        btnTimer = findViewById(R.id.btnTimer);

        btnAns0 = findViewById(R.id.btnAns0);
        btnAns1 = findViewById(R.id.btnAns1);
        btnAns2 = findViewById(R.id.btnAns2);
        btnAns3 = findViewById(R.id.btnAns3);

        btnPlayAgain = findViewById(R.id.btnPlayAgain);

        answers = new ArrayList<>();
    }

    public void actionGo(View v) {

        Button btnGo = findViewById(R.id.btnGo);
        btnGo.setVisibility(View.INVISIBLE);

        ConstraintLayout lytMain = findViewById(R.id.lytMain);
        lytMain.setVisibility(View.VISIBLE);

        playAgain(null);
    }

    public void playAgain(View v){

        score = 0;
        numberOfQuestions = 0;
        btnTimer.setText("30s");
        btnScore.setText("0/0");

        txtvResult.setText("");
        btnPlayAgain.setVisibility(View.INVISIBLE);

        finish = false;


        countDownTimer = new CountDownTimer(30100, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                btnTimer.setText(millisUntilFinished/1000 + "s");

            }

            @Override
            public void onFinish() {
                btnTimer.setText("0s");
                txtvResult.setText("Your Score " + score + "/" + numberOfQuestions);

                btnPlayAgain.setVisibility(View.VISIBLE);

                finish = true;

            }
        }.start();


        generateQuestion();
    }

    private void generateQuestion(){

        Random r = new Random();
        int num1 = r.nextInt(21); //0 - 20
        int num2 = r.nextInt(21);

        txtvSum.setText(num1 + " + " + num2);

        locationOfCorrectAnswer = r.nextInt(4);

        answers.clear();

        int incorrectAnswer = 0;
        for(int i = 0; i < 4; i++){

            if(i == locationOfCorrectAnswer){
                answers.add(num1+num2);
            }else{

                incorrectAnswer = r.nextInt(41);

                while(incorrectAnswer == (num1+num2)){
                    incorrectAnswer = r.nextInt(41);
                }

                answers.add(incorrectAnswer);
            }

        }

        btnAns0.setText(String.valueOf(answers.get(0)));
        btnAns1.setText(String.valueOf(answers.get(1)));
        btnAns2.setText(String.valueOf(answers.get(2)));
        btnAns3.setText(String.valueOf(answers.get(3)));


    }

    public void chooseAnswer(View v){

        if(!finish) {

            if (v.getTag().toString().equals(String.valueOf(locationOfCorrectAnswer))) {

                score++;
                txtvResult.setText("Correct!");

            } else {
                txtvResult.setText("Wrong!");
            }

            numberOfQuestions++;
            btnScore.setText(score + "/" + numberOfQuestions);

            generateQuestion();
        }

    }
}
