package mt.edu.mcast.restaurantorder;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
        /*
        Button btnPasta =findViewById(R.id.btnPasta);

        btnPasta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        */

    //onClick from GUI set it to the method

    public void menuClickOption(View v) {
        Intent intent = null;
        int requestCode = 0;

        if(v.getId() == R.id.btnPasta)
        {
            intent = new Intent(this,PastaActivity.class);
            requestCode = 1;
        }


        else if(v.getId() == R.id.btnPizza)
        {
            intent = new Intent(this, PizzaActivity.class);
            requestCode = 2;
        }

        startActivityForResult(intent, requestCode);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        if(resultCode == Activity.RESULT_OK)
        {
            String dish = intent.getStringExtra("dishName");
            double price = intent.getDoubleExtra("price", 10.50);

            EditText eText = findViewById(R.id.etOrder);

            eText.append("\n" + dish + "\t" + price);
        }
        else{
            //cancel was clicked
        }


    }
}
