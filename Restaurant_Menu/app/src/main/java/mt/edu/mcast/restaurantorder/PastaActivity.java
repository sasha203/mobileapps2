package mt.edu.mcast.restaurantorder;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class PastaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasta);
    }

    public void pastaSelection(View v)
    {
        String dish = "";
        double price = 0;
        int result = Activity.RESULT_CANCELED;

        switch (v.getId())
        {
            case R.id.btnCarbonara:
                dish = "Carbonara";
                price = 8.50;
                result = Activity.RESULT_OK;
                break;


        }

        Intent i = new Intent();
        i.putExtra("dishName", dish);
        i.putExtra("price", price);
        setResult(result, i);
        finish();

    }
}
