package e.student.eggtimer;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    SeekBar timerSeekBar;
    TextView timerTextView;
    Button btnControl;

    boolean isCounterActive;
    CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timerSeekBar = findViewById(R.id.sBarTimer);
        timerTextView = findViewById(R.id.txtTimer);
        btnControl = findViewById(R.id.btnControl);



        timerSeekBar.setMax(600); //10 mins
        timerSeekBar.setProgress(30);


        timerSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateTimer(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void controlTimer(View v){

        if(!isCounterActive) { //if false
            isCounterActive = true;
            timerSeekBar.setEnabled(false);
            btnControl.setText("Stop");

           countDownTimer = new CountDownTimer(timerSeekBar.getProgress() * 1000 + 100, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    updateTimer((int) millisUntilFinished / 1000);
                }

                @Override
                public void onFinish() {
                    timerTextView.setText("0.00");
                    MediaPlayer mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.bell);
                    mPlayer.start();
                    resetTimer();
                }
            }.start();
        }else{
            resetTimer();
        }
    }


    private void updateTimer(int secondsLeft){

        int remainingMinutes = (int)( secondsLeft/ 60);
        int seconds = secondsLeft - remainingMinutes * 60;

        String secondsString = Integer.toString(seconds);
        if(secondsLeft < 10){
            secondsString = "0" + seconds;
        }

        timerTextView.setText(remainingMinutes + ":" + secondsString);
    }


    private void resetTimer(){
        countDownTimer.cancel();
        timerSeekBar.setEnabled(true);
        timerSeekBar.setProgress(30);
        timerTextView.setText("0.30");
        btnControl.setText("Go");
        isCounterActive = false;
    }
}
